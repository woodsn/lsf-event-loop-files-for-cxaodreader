#include <stdlib.h>
#include <vector>
#include <fstream>
#include <getopt.h>
#include <boost/lexical_cast.hpp>
#include <boost/algorithm/string.hpp>

#include "TSystem.h"
#include "TFile.h"
#include "TPython.h"

#include "xAODRootAccess/Init.h"
#include "SampleHandler/SampleHandler.h"
#include "SampleHandler/ToolsDiscovery.h"
#include "SampleHandler/ScanDir.h"
#include "SampleHandler/DiskListLocal.h"
#include "SampleHandler/DiskListEOS.h"
#include "EventLoop/Job.h"

#include "EventLoop/DirectDriver.h"
#include "EventLoop/CondorDriver.h"
#include "EventLoop/SlurmDriver.h"
#include "EventLoop/Driver.h"
#include "EventLoop/LSFDriver.h"

#include "SampleHandler/Sample.h"
#include "xAODRootAccess/tools/TFileAccessTracer.h"

#include "CxAODTools/ConfigStore.h"

#include "CxAODReader_VVSemileptonic/AnalysisReaderVV.h"
#include "CxAODReader_VVSemileptonic/AnalysisReaderZV0Lep.h"
#include "CxAODReader_VVSemileptonic/AnalysisReaderWV1Lep.h"
#include "CxAODReader_VVSemileptonic/AnalysisReaderZV2Lep.h"

// TO RUN ON LYON BATCH
#include "SampleHandler/ToolsSplit.h"
#include "EventLoop/GEDriver.h"

void printUsage (std::ostream & os)
{
  os << "Usage: " << std::endl;
  os << "ReadCxAODVVAnalysis [ options ]" << std::endl;
  os << "   Options: " << std::endl;
  os << "    " << std::setw(24) << "-h | --help : print this text" << std::endl;
  os << "    " << std::setw(24) << "-c | --config <path> : override the config file" << std::endl;
  os << "    " << std::setw(24) << "-d | --driver : override the driver in config file" << std::endl;
  os << "    " << std::setw(24) << "-m | --maxEvents <int> : max total number of events to run"  << std::endl;
  os << "    " << std::setw(24) << "-n | --nEvtsPerWk <int> : max number of events assigned to each batch worker"  << std::endl;
  os << "    " << std::setw(24) << "-o | --outputDir <path> : override the outputDir value [default: submitDir]" << std::endl;
  os << "    " << std::setw(24) << "-s | --samples <s1,s2> : the sample list value"  << std::endl;
  os << "    " << std::setw(24) << "-w | --workerLoad <int> : number of files for each batch worker"  << std::endl;
  os << "    " << std::setw(24) << "-y | --yield <path> : override the initial sum of weights file to be used" << std::endl;
  os << "    " << std::setw(24) << "--nominalOnly [flag]: flag for running over nominal only"  << std::endl;
  os << "    " << std::setw(24) << "--override [flag]: flag for overriding the submit directory"  << std::endl;
}

int main(int argc, char* argv[]) {
  // flags for command line overrides
  bool override_config = false;
  bool override_driver = false;
  bool override_maxEvents = false;
  bool override_nEvtsPerWk = false;
  bool override_outputDir = false;
  bool override_samples = false;
  bool override_workerLoad = false;
  bool override_yield = false;
  // variables
  std::string config_path = "data/CxAODReader_VVSemileptonic/ZV2Lep.cfg";
  std::string driver = "direct";
  int max_events (-1);
  int nevents_per_worker (-1);
  std::string output_dir = "submitDir";
  std::string samples_str; // to be converted to vector<string>
  std::vector<std::string> sample_names;
  int nfiles_per_worker (5);
  std::string yield_path;
  int flag_nominal_only (-1);
  int flag_override_previous (-1);
  // construct options
  static struct option long_options[] = {
    {"help",       no_argument,       0, 'h'},
    {"config",     required_argument, 0, 'c'},
    {"driver",     required_argument, 0, 'd'},
    {"maxEvents", required_argument, 0, 'm'},
    {"nEvtsPerWk", required_argument, 0, 'n'},
    {"outputDir",  required_argument, 0, 'o'},
    {"samples",    required_argument, 0, 's'},
    {"workerLoad", required_argument, 0, 'w'},
    {"yield",     required_argument, 0, 'y'},
    {"nominalOnly", no_argument, &flag_nominal_only ,      1},
    {"override",    no_argument, &flag_override_previous , 1},
    {0, 0, 0, 0}
  };
  int option_index =0;
  int c = 0;
  // get arguments from command line
  while ((c = getopt_long(argc, argv, "hc:d:m:n:o:s:w:y:", long_options, &option_index ) ) != -1 )
  {
    switch(c)
    {
      case 0:
        if (long_options[option_index].flag != 0)
          break;
      case 'h':
        printUsage(std::cout);
        return 0;
      case 'c':
        override_config = true;
        config_path = std::string (optarg);
        break;
      case 'd':
        override_driver = true;
        driver = std::string (optarg);
        break;
      case 'm':
        override_maxEvents = true;
        max_events = boost::lexical_cast<int>(optarg);
        break;
      case 'n':
        override_nEvtsPerWk = true;
        nevents_per_worker = boost::lexical_cast<int>(optarg);
        break;
      case 'o':
        override_outputDir = true;
        output_dir = std::string(optarg);
        break;
      case 's':
        override_samples = true;
        samples_str = std::string(optarg);
        break;
      case 'w':
        nfiles_per_worker = boost::lexical_cast<double>(optarg);
        override_workerLoad = true;
        break;
      case 'y':
        yield_path = std::string(optarg);
        override_yield = !yield_path.empty();
        break;
      default:
        printUsage(std::cout);
        return EXIT_FAILURE;
    }
  }

  // read run config
  if (override_config) { Info("ReadCxAODVVAnalysis","Overriding with config file : %s", config_path.c_str()); }
  static ConfigStore* config = ConfigStore::createStore(config_path);

  // enable failure on unchecked status codes
  bool enable_failure = false;
  config->getif<bool>("failUncheckedStatusCodes", enable_failure);
  if (enable_failure) {
    xAOD::TReturnCode::enableFailure();
    StatusCode::enableFailure();
    CP::CorrectionCode::enableFailure();
    CP::SystematicCode::enableFailure();
  }

  // Set up the job for xAOD access:
  xAOD::Init().ignore();

  // Construct the samples to run on:
  sample_names = {"data", "data15", "data16", "data17",
    "HZZllqq", "HZZvvqq", "HVTWZ", "RS_G_ZZ",
    "ZeeB", "ZeeC", "ZeeL",
    "ZmumuB", "ZmumuC", "ZmumuL",
    "ZtautauB", "ZtautauC", "ZtautauL",
    "ZnunuB", "ZnunuC", "ZnunuL",
    "WenuB", "WenuC", "WenuL",
    "WmunuB", "WmunuC", "WmunuL",
    "WtaunuB", "WtaunuC", "WtaunuL",
    "ttbar", "ttbar_nonallhad", "singletop_t", "singletop_s", "singletop_Wt",
    "ttbar_PwHerwigppEG", "ttbar_PwPyEG", "ttbar_aMcAtNloHerwigppEG",
    "ZHvv125", "WH125", "ZHll125", "ZHll125J_MINLO",
    "bbA", "ggA", "HVT", "HbbjjaSM125", "HbbjjaHEFT125", "NonResbbjja", "ZbbjjaEWK", "ZbbjjaQCD",
    "Wenu_MG", "Wmunu_MG", "Wtaunu_MG",
    "Wenu_Pw", "Wmunu_Pw", "Wtaunu_Pw",
    "Zee_MG", "Zmumu_MG", "Ztautau_MG", "Znunu_MG",
    "Zee_Pw", "Zmumu_Pw", "Ztautau_Pw", "Znunu_Pw",
    "WW", "WZ", "ZZ",
    "WW_improved", "WZ_improved", "ZZ_improved",
    "ZeeB_v22", "ZeeC_v22", "ZeeL_v22",
    "ZmumuB_v22", "ZmumuC_v22", "ZmumuL_v22",
    "ZtautauB_v22", "ZtautauC_v22", "ZtautauL_v22",
    "ZnunuB_v22", "ZnunuC_v22", "ZnunuL_v22",
    "WenuB_v22", "WenuC_v22", "WenuL_v22",
    "WmunuB_v22", "WmunuC_v22", "WmunuL_v22",
    "WtaunuB_v22", "WtaunuC_v22", "WtaunuL_v22",
    "HVT_0500", "HVT_0600", "HVT_0700", "HVT_0800", "HVT_0900", "HVT_1000",
    "HVT_1100", "HVT_1200", "HVT_1300", "HVT_1400", "HVT_1500", "HVT_1600",
    "HVT_1700", "HVT_1800", "HVT_1900", "HVT_2000", "HVT_2200", "HVT_2400",
    "HVT_2600", "HVT_2800", "HVT_3000", "HVT_3500", "HVT_4000", "HVT_4500", "HVT_5000"
  };

  // Override sample list from config
  if (override_samples) {
    Info("ReadCxAODVVAnalysis", "Overriding samples with \"%s\"", samples_str.c_str());
    // convert samples_str (in the format of e.g. "WW,WZ,ZZ" to vector
    boost::split( sample_names, samples_str, boost::is_any_of(","), boost::token_compress_on );
  }
  else {
    sample_names = config->get<std::vector<std::string> >("samples");
  }

  // Possibility to skip some of the samples above
  // Read bools from config file and if false remove sample from vector
  std::vector<std::string>::iterator itr;
  for (itr = sample_names.begin(); itr != sample_names.end();) {
    bool includeSample = true;
    config->getif<bool>(*itr, includeSample);
    if ( !includeSample ) itr = sample_names.erase(itr);
    else ++itr;
  }

  std::string dataset_dir = config->get<std::string>("dataset_dir");

  bool eos;
  std::string prefix = "/eos/";
  if ( dataset_dir.substr(0, prefix.size()).compare(prefix) == 0 ) {
    eos = true;
    std::cout << "Will read datasets from EOS directory " << dataset_dir << std::endl;
  }
  else {
    eos = false;
    std::cout << "Will read datasets from local directory " << dataset_dir << std::endl;
  }

  // Query - I had to put each background in a separate directory
  // for samplehandler to have sensible sample names etc.
  // Is it possible to define a search string for directories and assign all those to a sample name?
  // SH::scanDir (sampleHandler, list); // apparently it will come

  // create the sample handler
  SH::SampleHandler sampleHandler;
  std::cout << "Scanning samples:" << std::endl;
  for (unsigned int isamp(0) ; isamp<sample_names.size() ; isamp++) {
    std::string sample_name(sample_names.at(isamp));
    std::string sample_dir(dataset_dir+"/"+sample_name);

    if (!eos) {
      bool direxists=gSystem->OpenDirectory (sample_dir.c_str());
      if (!direxists) {
        std::cout << " No sample exists: " << sample_name << " , skipping: "  << sample_dir << std::endl;
        continue;
      }
    }

    std::string inFiles = "";
    config->getif<std::string>("inFiles", inFiles);

    // eos, local disk or afs
    if (eos) {
      SH::DiskListEOS list(sample_dir,"root://eosatlas.cern.ch:/"+sample_dir );
      // tuples are downloaded to same directory as CxAOD so specify CxAOD root file pattern
      if ( inFiles.empty() )  {
        SH::ScanDir()
          .samplePattern("*CxAOD*")
          .sampleName(sample_name)
          .scan(sampleHandler,list);
      } else {
        SH::ScanDir()
          .samplePattern(inFiles)
          .sampleName(sample_name)
          .scan(sampleHandler,list);
      }
    } else {
      SH::DiskListLocal list(sample_dir);
      // tuples are downloaded to same directory as CxAOD so specify CxAOD root file pattern
      if ( inFiles.empty() ) {
        SH::ScanDir()
          .samplePattern("*CxAOD*")
          .sampleName(sample_name)
          .scan(sampleHandler,list);
      } else {
        SH::ScanDir()
          .samplePattern(inFiles)
          .sampleName(sample_name)
          .scan(sampleHandler,list);
      }
    }

    SH::Sample* sample_ptr = sampleHandler.get(sample_name);
    sample_ptr->meta()->setString("SampleID",sample_name);
    int nsampleFiles=sample_ptr->numFiles();

    std::cout<<"Sample name "<<sample_name<<" with nfiles : "<<nsampleFiles<<std::endl;
  }

  // Split samples and run nEventsPerJob for each subjob
  if (override_nEvtsPerWk) { Info("ReadCxAODVVAnalysis", "Overriding nEventsPerJob with %d", nevents_per_worker); }
  else { config->getif<int>("nEventsPerJob", nevents_per_worker); }
  if (nevents_per_worker > 0 ) {
    nevents_per_worker = nevents_per_worker < 10000 ? 10000 : nevents_per_worker;
    SH::scanNEvents(sampleHandler);
    sampleHandler.setMetaDouble(EL::Job::optEventsPerWorker, nevents_per_worker);
  }

  // Set the name of the input TTree. It's always "CollectionTree"
  // for xAOD files.
  sampleHandler.setMetaString("nc_tree", "CollectionTree");

  // Print what we found:
  std::cout << "Printing sample handler contents:" << std::endl;
  sampleHandler.print();

  // generate yield file from input CxAODs
  bool generateYieldFile (false);
  config->getif<bool>("generateYieldFile", generateYieldFile);
  if (override_yield) {
    Info("ReadCxAODVVAnalysis","Overriding yieldFile with %s", yield_path.c_str());
    // Set the "yieldFile" filed in the config to our yield_path
    config->put<std::string>("yieldFile", yield_path, true);
    // set it to false so that it doesn't cause trouble in AnalysisReaderVV.cxx
    generateYieldFile = false;
  }
  else if (generateYieldFile) {
    // write file list to temporary text file
    std::ofstream file;
    file.open("fileList_temp.txt");
    SH::SampleHandler::iterator it = sampleHandler.begin();
    SH::SampleHandler::iterator it_end = sampleHandler.end();
    for (; it != it_end; it++) {
      SH::Sample* sample = *it;
      for (unsigned int i = 0; i < sample->numFiles(); i++) {
        file << sample->fileName(i) << std::endl;
      }
    }
    file.close();
    // sort file list and generate yield file
    system("sort fileList_temp.txt > fileList.txt");
    TPython::Exec("import sys");
    TPython::Exec("sys.argv=['dummy', '0', '0', 'fileList.txt']");
    TPython::LoadMacro("./CxAODReader_VVSemileptonic/scripts/count_Nentry_SumOfWeight.py");
    yield_path = (const char*) TPython::Eval("out_file_md5");
    system("rm fileList_temp.txt fileList.txt");
  }

  // Create an EventLoop job:
  EL::Job job;
  job.sampleHandler(sampleHandler);

  // turn off sending xAOD summary access report (to be discussed)
  bool enable_data_submission = true;
  config->getif<bool>("enableDataSubmission", enable_data_submission);
  xAOD::TFileAccessTracer::enableDataSubmission (enable_data_submission);

  // remove submit dir before running the job
  if (flag_override_previous == 1) { job.options()->setDouble(EL::Job::optRemoveSubmitDir, 1); }

  // create algorithm, set job options, maniplate members and add our analysis to the job:
  AnalysisReader* algorithm;
  std::string analysis_type = config->get<std::string>("analysisType");
  if ( analysis_type == "2lep" ) {
    algorithm = new AnalysisReaderZV2Lep ();
  }
  else if ( analysis_type == "1lep" ) {
    algorithm = new AnalysisReaderWV1Lep ();
  }
  else if ( analysis_type == "0lep" ) {
    algorithm = new AnalysisReaderZV0Lep ();
  }
  else {
    Error("hsg5framework", "Unknown analysis type '%s'", analysis_type.c_str());
    return 0;
  }

  algorithm->setConfig(config);
  algorithm->setSumOfWeightsFile(yield_path);

  //limit number of events to maxEvents - set in config
  if (override_maxEvents) {
    Info("ReadCxAODVVAnalysis","Overriding maxEvents : %i", max_events);
    config->put<int>("maxEvents", max_events, true);
  }
  else {
    config->getif<int>("maxEvents", max_events);
  }
  job.options()->setDouble (EL::Job::optMaxEvents, max_events);

  // https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/EventLoop#Access_the_Data_Through_xAOD_EDM
  bool nominalOnly (false);
  if (flag_nominal_only == 1) {
    Info("ReadCxAODVVAnalysis", "Overriding config with nominalOnly = true");
    config->put<bool>("nominalOnly", true, true);
  }
  config->getif<bool>("nominalOnly", nominalOnly);
  if (nominalOnly) {
    // branch access shows better performance for CxAODReader with Nominal only
    job.options()->setString (EL::Job::optXaodAccessMode, EL::Job::optXaodAccessMode_branch);
  }
  else {
    // branch access cannot read shallow copies, so we need class access in case reading systematics
    job.options()->setString (EL::Job::optXaodAccessMode, EL::Job::optXaodAccessMode_class);
  }

  // add algorithm to job
  job.algsAdd(algorithm);

  // Number of files to submit per worker (job) if the option is not overridden.
  if (!override_workerLoad) { config->getif<int>("nFilesPerJob", nfiles_per_worker); }
  if (nfiles_per_worker > 0) { job.options()->setDouble (EL::Job::optFilesPerWorker, nfiles_per_worker); }

  // Display information if the submitDir is overridden.
  if (override_outputDir) { Info("ReadCxAODVVAnalysis", "Overriding output directory with %s", output_dir.c_str()); }

  // Figure out which driver to use
  if (override_driver) { Info("ReadCxAODVVAnalysis", "Overriding driver with : %s", driver.c_str()); }
  else { config->getif<std::string>("driver", driver); }

  // Initialize the driver
  if (driver=="direct"){
    EL::DirectDriver*  eldriver = new EL::DirectDriver;
    eldriver->submit(job, output_dir);
  }
//  else if (driver=="proof") {
//    EL::ProofDriver* eldriver = new EL::ProofDriver;
//    eldriver->submit(job, output_dir);
//  }
  else if ( driver == "LSF") {
      EL::LSFDriver* eldriver = new EL::LSFDriver;
      eldriver->options()->setString (EL::Job::optSubmitFlags, "-L /bin/bash");
      eldriver->shellInit = "export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase && source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh";
      std::string bQueue = "1nh";
      config->getif<std::string>("bQueue",bQueue);
      job.options()->setString (EL::Job::optSubmitFlags, ("-W "+bQueue).c_str()); //1nh
      eldriver->submitOnly(job, output_dir);
   }
  else if ( driver == "condor" ) {
    EL::CondorDriver* eldriver = new EL::CondorDriver;
    eldriver->shellInit = "export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase && export PATH=${PATH}:/usr/sbin:/sbin && source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh";

    // mdacunha: it is strange but it works for each user
    // --> will create something like /eos/user/m/mdacunha/CONDOR_output/ can be changed in config file
    char* userForCondor = std::getenv ("USER");
    string eosPATH = "/eos/user/";
    eosPATH += userForCondor[0];
    eosPATH += "/";
    eosPATH += userForCondor;
    eosPATH += "/CONDOR_output/";

    config->getif<std::string>("eosPATH",eosPATH);
    string createDir = "mkdir -p " + eosPATH; system(createDir.c_str()); // The directory must be created to store the submissions.
    eosPATH += output_dir + "/";
    // config->getif<std::string>("eosFolderUser",eosPATH);
    std::cout << "The output (fetch and status) will be saved in directory " << eosPATH.c_str() << std::endl;

    TString configs4condor = "";

    std::string cQueue = "microcentury"; // == 1 hour
    config->getif<std::string>("cQueue",cQueue);

    configs4condor += "+JobFlavour = \"" + cQueue + "\"\n";

    //Next 3-lines temporary while some batch servers are migrating installations
    configs4condor.Append(" \n");
    configs4condor.Append("requirements = (OpSysAndVer =?= \"CentOS7\")");
    configs4condor.Append(" \n");

    job.options()->setString (EL::Job::optCondorConf, configs4condor.Data());

    eldriver->submitOnly(job, output_dir,eosPATH.c_str());
    // eldriver->submitOnly(job, output_dir);
    // eldriver->submitOnly(job, "/eos/user/m/mdacunha/CONDOR_output/test_d_newCondor_03/");

  }

  return 0;
}
